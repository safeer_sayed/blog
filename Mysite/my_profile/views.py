from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import auth
from my_profile.forms import UserForm, UserProfileForm,CommentsForm,PostsForm
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from my_profile.models import User_Profile,Posts,Comments
from django.core.mail import send_mail
from django.http import Http404

# Create your views here.

def login(request):
    return render_to_response('index1.html')

def register_user(request):
    context = RequestContext(request)
    registered = False
    user_form = UserForm()
    profile_form = UserProfileForm()
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()
            registered = True
        if registered:
            return HttpResponseRedirect('/mysites/registeration_successful')
        
    return render_to_response('new_registration.html',
        {'user_form': user_form, 'profile_form': profile_form,
        'registered': registered},
              context)

def register_success(request):
    return render_to_response("successful_registration.html")

@csrf_exempt
def update_success(request):
    return render_to_response("update.html")

@csrf_exempt
def auth_view(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        auth.login(request, user)
        email_subject = 'Account confirmation'
        email_body = "Hey %s, thanks for Login."%(username)
        send_mail(email_subject, email_body, 'safeer.sayone@gmail.com',['safeersayed1990@gmail.com'], fail_silently=False)
        return HttpResponseRedirect('/mysites/my_home')
    else:
        return HttpResponseRedirect('/mysites/invalid_page')

@csrf_exempt
def invalid_login(request):
    return render_to_response("invalid_page.html")

@csrf_exempt
def loggedin(request):
    details = User_Profile.objects.get(user=request.user)
    return render_to_response("my_home.html",
        {"full_name": details.name, 'address': details.address,
        'phone': details.phone, 'age': details.age})

def eventlist(request):
    posts=Posts.objects.all()
    context=RequestContext(request)
    return render_to_response('posts.html',{'post':posts},context)

@csrf_exempt
def change_profile(request):
    context=RequestContext(request)
    user = request.user
    p = User_Profile.objects.get(user=user)
    print "lkblkjbhlkjhblkbhj"
    if request.method == "POST":
      p.name = request.POST['name']
      print request.POST['name'], "======"
      p.address = request.POST['address']
      p.phone = request.POST['phone']
      p.age = request.POST['age']
      p.save()
    return render_to_response('change_profile.html',{'profile':p,"user": user},context)
        

@csrf_exempt
def add_comments(request,p_id):
    post = Posts.objects.get(pk=p_id)
    if request.method == 'POST':
      form = CommentsForm(request.POST)
      if form.is_valid():
        p = form.save(commit=False)
        p.name = request.user
        p.post =  post
        p.save()
    context = RequestContext(request)
    c = CommentsForm()
    return render_to_response('add_comments.html',
              {'post': post, 'textform': c
                 },context
               )



