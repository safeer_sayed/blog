from django.db import models

# Create your models here.

from django.contrib.auth.models import User 

class User_Profile(models.Model):

    user = models.OneToOneField(User)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=30)
    age = models.CharField(max_length=5)
    def __str__(self):
        return "%s,%s,%s,%s" % (self.name,self.address,self.phone,self.age)

class Posts(models.Model):		
    title = models.CharField(max_length=50)
    text = models.TextField()
    def __str__(self):
    	return self.title
		
class Comments(models.Model):
	name = models.CharField(max_length=50,null=True)
	text = models.TextField()
	post = models.ForeignKey(Posts)
	def __str__(self):
		return self.text
		
		
