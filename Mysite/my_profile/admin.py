from django.contrib import admin
from my_profile.models import User_Profile
from my_profile.models import Posts
from my_profile.models import Comments

admin.site.register(User_Profile)
admin.site.register(Posts)
admin.site.register(Comments)
# Register your models here.
