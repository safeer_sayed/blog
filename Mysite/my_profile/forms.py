from django import forms
from django.contrib.auth.models import User
from my_profile.models import User_Profile
from my_profile.models import Posts
from my_profile.models import Comments


class UserForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:

        model = User
        fields = ('username', 'password')


class UserProfileForm(forms.ModelForm):

    class Meta:
        model = User_Profile
        fields = ('name', 'address', 'phone', 'age')

class CommentsForm(forms.ModelForm):
	
	class Meta:
		model = Comments
		fields = ('text',)

class PostsForm(forms.ModelForm):
    class Meta:
    	model=Posts
    	fields =('title','text')
						