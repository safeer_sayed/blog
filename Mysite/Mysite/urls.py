"""Mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^mysites/login$', 'my_profile.views.login',name="login"),
    url(r'^mysites/registration$', 'my_profile.views.register_user',name="register"),
    url(r'^mysites/registeration_successful$', 'my_profile.views.register_success',name='success'),
    url(r'^mysites/auth$', 'my_profile.views.auth_view',name='index'),
    url(r'^mysites/invalid_page$', 'my_profile.views.invalid_login'),
    url(r'^mysites/my_home$', 'my_profile.views.loggedin', name="home"),
    url(r'^mysites/$', 'my_profile.views.login',name="logout"),
    url(r'^mysites/view_posts$', 'my_profile.views.eventlist',name="comments"),
    url(r'^(?P<p_id>[0-9]+)', 'my_profile.views.add_comments',name='logg'),
    url(r'^mysites/change_profile$', 'my_profile.views.change_profile',name='change'),
    url(r'^mysites/update_successful$', 'my_profile.views.update_success',name='update'),
]
 #url(r'^mysites/edit_profile/(<p_id>[0-9]+)$', 'my_profile.views.change_profile',name='change'),